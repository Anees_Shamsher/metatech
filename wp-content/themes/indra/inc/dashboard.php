<?php
/**
 * Builds our admin page.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'indra_create_menu' ) ) {
	add_action( 'admin_menu', 'indra_create_menu' );
	/**
	 * Adds our "Indra" dashboard menu item
	 *
	 */
	function indra_create_menu() {
		$indra_page = add_theme_page( 'Indra', 'Indra', apply_filters( 'indra_dashboard_page_capability', 'edit_theme_options' ), 'indra-options', 'indra_settings_page' );
		add_action( "admin_print_styles-$indra_page", 'indra_options_styles' );
	}
}

if ( ! function_exists( 'indra_options_styles' ) ) {
	/**
	 * Adds any necessary scripts to the Indra dashboard page
	 *
	 */
	function indra_options_styles() {
		wp_enqueue_style( 'indra-options', get_template_directory_uri() . '/css/admin/admin-style.css', array(), INDRA_VERSION );
	}
}

if ( ! function_exists( 'indra_settings_page' ) ) {
	/**
	 * Builds the content of our Indra dashboard page
	 *
	 */
	function indra_settings_page() {
		?>
		<div class="wrap">
			<div class="metabox-holder">
				<div class="indra-masthead clearfix">
					<div class="indra-container">
						<div class="indra-title">
							<a href="<?php echo esc_url(INDRA_THEME_URL); ?>" target="_blank"><?php esc_html_e( 'Indra', 'indra' ); ?></a> <span class="indra-version"><?php echo INDRA_VERSION; ?></span>
						</div>
						<div class="indra-masthead-links">
							<?php if ( ! defined( 'INDRA_PREMIUM_VERSION' ) ) : ?>
								<a class="indra-masthead-links-bold" href="<?php echo esc_url(INDRA_THEME_URL); ?>" target="_blank"><?php esc_html_e( 'Premium', 'indra' );?></a>
							<?php endif; ?>
							<a href="<?php echo esc_url(INDRA_WPKOI_AUTHOR_URL); ?>" target="_blank"><?php esc_html_e( 'WPKoi', 'indra' ); ?></a>
                            <a href="<?php echo esc_url(INDRA_DOCUMENTATION); ?>" target="_blank"><?php esc_html_e( 'Documentation', 'indra' ); ?></a>
						</div>
					</div>
				</div>

				<?php
				/**
				 * indra_dashboard_after_header hook.
				 *
				 */
				 do_action( 'indra_dashboard_after_header' );
				 ?>

				<div class="indra-container">
					<div class="postbox-container clearfix" style="float: none;">
						<div class="grid-container grid-parent">

							<?php
							/**
							 * indra_dashboard_inside_container hook.
							 *
							 */
							 do_action( 'indra_dashboard_inside_container' );
							 ?>

							<div class="form-metabox grid-70" style="padding-left: 0;">
								<h2 style="height:0;margin:0;"><!-- admin notices below this element --></h2>
								<form method="post" action="options.php">
									<?php settings_fields( 'indra-settings-group' ); ?>
									<?php do_settings_sections( 'indra-settings-group' ); ?>
									<div class="customize-button hide-on-desktop">
										<?php
										printf( '<a id="indra_customize_button" class="button button-primary" href="%1$s">%2$s</a>',
											esc_url( admin_url( 'customize.php' ) ),
											esc_html__( 'Customize', 'indra' )
										);
										?>
									</div>

									<?php
									/**
									 * indra_inside_options_form hook.
									 *
									 */
									 do_action( 'indra_inside_options_form' );
									 ?>
								</form>

								<?php
								$modules = array(
									'Backgrounds' => array(
											'url' => INDRA_THEME_URL,
									),
									'Blog' => array(
											'url' => INDRA_THEME_URL,
									),
									'Colors' => array(
											'url' => INDRA_THEME_URL,
									),
									'Copyright' => array(
											'url' => INDRA_THEME_URL,
									),
									'Disable Elements' => array(
											'url' => INDRA_THEME_URL,
									),
									'Demo Import' => array(
											'url' => INDRA_THEME_URL,
									),
									'Hooks' => array(
											'url' => INDRA_THEME_URL,
									),
									'Import / Export' => array(
											'url' => INDRA_THEME_URL,
									),
									'Menu Plus' => array(
											'url' => INDRA_THEME_URL,
									),
									'Page Header' => array(
											'url' => INDRA_THEME_URL,
									),
									'Secondary Nav' => array(
											'url' => INDRA_THEME_URL,
									),
									'Spacing' => array(
											'url' => INDRA_THEME_URL,
									),
									'Typography' => array(
											'url' => INDRA_THEME_URL,
									),
									'Elementor Addon' => array(
											'url' => INDRA_THEME_URL,
									)
								);

								if ( ! defined( 'INDRA_PREMIUM_VERSION' ) ) : ?>
									<div class="postbox indra-metabox">
										<h3 class="hndle"><?php esc_html_e( 'Premium Modules', 'indra' ); ?></h3>
										<div class="inside" style="margin:0;padding:0;">
											<div class="premium-addons">
												<?php foreach( $modules as $module => $info ) { ?>
												<div class="add-on activated indra-clear addon-container grid-parent">
													<div class="addon-name column-addon-name" style="">
														<a href="<?php echo esc_url( $info[ 'url' ] ); ?>" target="_blank"><?php echo esc_html( $module ); ?></a>
													</div>
													<div class="addon-action addon-addon-action" style="text-align:right;">
														<a href="<?php echo esc_url( $info[ 'url' ] ); ?>" target="_blank"><?php esc_html_e( 'More info', 'indra' ); ?></a>
													</div>
												</div>
												<div class="indra-clear"></div>
												<?php } ?>
											</div>
										</div>
									</div>
								<?php
								endif;

								/**
								 * indra_options_items hook.
								 *
								 */
								do_action( 'indra_options_items' );
								?>
							</div>

							<div class="indra-right-sidebar grid-30" style="padding-right: 0;">
								<div class="customize-button hide-on-mobile">
									<?php
									printf( '<a id="indra_customize_button" class="button button-primary" href="%1$s">%2$s</a>',
										esc_url( admin_url( 'customize.php' ) ),
										esc_html__( 'Customize', 'indra' )
									);
									?>
								</div>

								<?php
								/**
								 * indra_admin_right_panel hook.
								 *
								 */
								 do_action( 'indra_admin_right_panel' );

								  ?>
                                
                                <div class="wpkoi-doc">
                                	<h3><?php esc_html_e( 'Indra documentation', 'indra' ); ?></h3>
                                	<p><?php esc_html_e( 'If You`ve stuck, the documentation may help on WPKoi.com', 'indra' ); ?></p>
                                    <a href="<?php echo esc_url(INDRA_DOCUMENTATION); ?>" class="wpkoi-admin-button" target="_blank"><?php esc_html_e( 'Indra documentation', 'indra' ); ?></a>
                                </div>
                                
                                <div class="wpkoi-social">
                                	<h3><?php esc_html_e( 'WPKoi on Facebook', 'indra' ); ?></h3>
                                	<p><?php esc_html_e( 'If You want to get useful info about WordPress and the theme, follow WPKoi on Facebook.', 'indra' ); ?></p>
                                    <a href="<?php echo esc_url(INDRA_WPKOI_SOCIAL_URL); ?>" class="wpkoi-admin-button" target="_blank"><?php esc_html_e( 'Go to Facebook', 'indra' ); ?></a>
                                </div>
                                
                                <div class="wpkoi-review">
                                	<h3><?php esc_html_e( 'Help with You review', 'indra' ); ?></h3>
                                	<p><?php esc_html_e( 'If You like Indra theme, show it to the world with Your review. Your feedback helps a lot.', 'indra' ); ?></p>
                                    <a href="<?php echo esc_url(INDRA_WORDPRESS_REVIEW); ?>" class="wpkoi-admin-button" target="_blank"><?php esc_html_e( 'Add my review', 'indra' ); ?></a>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

if ( ! function_exists( 'indra_admin_errors' ) ) {
	add_action( 'admin_notices', 'indra_admin_errors' );
	/**
	 * Add our admin notices
	 *
	 */
	function indra_admin_errors() {
		$screen = get_current_screen();

		if ( 'appearance_page_indra-options' !== $screen->base ) {
			return;
		}

		if ( isset( $_GET['settings-updated'] ) && 'true' == $_GET['settings-updated'] ) {
			 add_settings_error( 'indra-notices', 'true', esc_html__( 'Settings saved.', 'indra' ), 'updated' );
		}

		if ( isset( $_GET['status'] ) && 'imported' == $_GET['status'] ) {
			 add_settings_error( 'indra-notices', 'imported', esc_html__( 'Import successful.', 'indra' ), 'updated' );
		}

		if ( isset( $_GET['status'] ) && 'reset' == $_GET['status'] ) {
			 add_settings_error( 'indra-notices', 'reset', esc_html__( 'Settings removed.', 'indra' ), 'updated' );
		}

		settings_errors( 'indra-notices' );
	}
}
