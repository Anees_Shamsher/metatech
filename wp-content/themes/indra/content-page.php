<?php
/**
 * The template used for displaying page content in page.php
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php indra_article_schema( 'CreativeWork' ); ?>>
	<div class="inside-article">
		<?php
		/**
		 * indra_before_content hook.
		 *
		 *
		 * @hooked indra_featured_page_header_inside_single - 10
		 */
		do_action( 'indra_before_content' );

		if ( indra_show_title() ) : ?>

			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
			</header><!-- .entry-header -->

		<?php endif;

		/**
		 * indra_after_entry_header hook.
		 *
		 *
		 * @hooked indra_post_image - 10
		 */
		do_action( 'indra_after_entry_header' );
		?>

		<div class="entry-content" itemprop="text">
			<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'indra' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->

		<?php
		/**
		 * indra_after_content hook.
		 *
		 */
		do_action( 'indra_after_content' );
		?>
	</div><!-- .inside-article -->
</article><!-- #post-## -->
