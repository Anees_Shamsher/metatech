<?php
/**
 * The template for displaying the footer.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

	</div><!-- #content -->
</div><!-- #page -->

<?php
/**
 * indra_before_footer hook.
 *
 */
do_action( 'indra_before_footer' );
?>

<div <?php indra_footer_class(); ?>>
	<?php
	/**
	 * indra_before_footer_content hook.
	 *
	 */
	do_action( 'indra_before_footer_content' );

	/**
	 * indra_footer hook.
	 *
	 *
	 * @hooked indra_construct_footer_widgets - 5
	 * @hooked indra_construct_footer - 10
	 */
	do_action( 'indra_footer' );

	/**
	 * indra_after_footer_content hook.
	 *
	 */
	do_action( 'indra_after_footer_content' );
	?>
</div><!-- .site-footer -->

<?php
/**
 * indra_after_footer hook.
 *
 */
do_action( 'indra_after_footer' );

wp_footer();
?>

</body>
</html>
