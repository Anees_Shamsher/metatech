<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'metatech' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>y5Istc^7el)qYnsyqgG0BoJdii9pZN^J)2U6{:F6.IH>&`~x%O7. |)(_(C[q+A' );
define( 'SECURE_AUTH_KEY',  ',J)H9K5Ad]+[p+g-85Hu]:S*9gD1O@$,J~g1-0fB~5L/UryB%cr(-0Ehp,,jK)cL' );
define( 'LOGGED_IN_KEY',    '4BQJ4x.PvURs{WXGik0F0Y@&21R!Fuy8PCNcdCLh=<RIs(&Y@P!Cg2pthb]4Yk$j' );
define( 'NONCE_KEY',        '.C!f_n<,6_9.d nsa&h>*Bt7:kv=2:-W)v(^(&vmK~O8Wg8lc%1A[nJ@+]SeJ^/p' );
define( 'AUTH_SALT',        'xZ[BkHP8#|{|t>n{q+twOfEJ`n%4m q,6m2Ah*O{0,a:wEp*gYF(-M5IB}]V@h%@' );
define( 'SECURE_AUTH_SALT', '=4;, sP&}Un2^?`SXA0u+ cqRWh={G+/Qk6fb{-oW(wdqYqu%,^G7+C}.]24bdiO' );
define( 'LOGGED_IN_SALT',   'tp[CLiQt~1Q=p6<Py9iDis!Wyy-9$A-lT&vQ!CY(pURPJb|Ihmv@81kisjnB6Owh' );
define( 'NONCE_SALT',       'cHd7XL FPJza-s_#rJm{h+N[Xu1M}zP~`^2hTTg Q$PZz3X-)JQJzs{e.3va5-|X' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ask_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
